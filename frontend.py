#!/usr/bin/python
# frontend - Michael P. Reilly Copyright 2014-2016 All rights reserved
# A front-end to push butler changes to various environments without
# full access.  Limit to non-production servers through this tool.

import contextlib
import logging
import os
import sys
import subprocess

HomeDir = os.path.expanduser('~')
RootDir = os.path.join(HomeDir, 'butler')

def find_longest_common(choices):
    s = []
    values = choices[:]
    first = values[0]
    del values[0]
    i = 0
    while i < len(first):
        c = first[i]
        for m in values:
            if m[i] != c:
                return ''.join(s)
        else:
            s.append( c )
        i += 1
    # this would return the whole first string
    return ''.join(s)

class strbuf(object):
    def __init__(self, initial=''):
        self.data = []
        for c in initial:
            self.append(c)
    def __str__(self):
        return ''.join(self.data)
    def __eq__(self, other):
        return str(self) == other
    def __lt__(self, other):
        return str(self) < other
    def __hash__(self):
        return hash(str(self))
    def __len__(self):
        return len(self.data)
    def __iter__(self):
        return iter(self.data)
    def __getitem__(self, pos):
        return self.data[pos]
    def __setitem__(self, pos, value):
        assert isinstance(value, str) and len(value) == 1, 'only single characters can be added'
        self.data[pos] = value
    def __delitem__(self, pos):
        del self.data[pos]
    def __getslice__(self, i, j):
        return self.data[i:j]
    def __setslice__(self, i, j, value):
        assert isinstance(value, str), 'only strings can be sliced'
        self.data[i:j] = list(value)
    def __delslice__(self, i, j):
        del self.data[i:j]
    def append(self, value):
        assert isinstance(value, str) and len(value) == 1, 'only single characters can be added'
        self.data.append(value)
    def extend(self, value):
        assert isinstance(value, str), 'only strings can be extended'
        self.data.extend(list(value))
    def index(self, c):
        return self.data.index(c)
    def find(self, c):
        try:
            return self.data.index(c)
        except IndexError:
            return -1


class Prompter(object):
    stdin = stdout = None
    def __init__(self, text, default=None, choices=None, help=None):
        self.text = text
        self.default = default
        self.choices = choices
        self.helptext = help
        self.logger = logging.getLogger('prompt')
        self.logger.debug('Prompter(%s, %s, %s)', text, default, choices)
        self.setstreams()

    @classmethod
    def setstreams(cls):
        if cls.stdin is None and cls.stdout is None:
            if sys.stdin.isatty():
                cls.stdin = sys.stdin
                cls.stdout = sys.stdout
            else:
                cls.stdin = cls.stdout = open('/dev/tty', 'w+')

    def make_prompt(self):
        if self.default:
            return '%s [%s]: ' % (self.text, self.default)
        else:
            return '%s: ' % self.text

    def handle_tab(self, value):
        ofd = self.stdout.fileno()
        if self.choices:
            text = str(value)
            self.logger.debug('text = %s', repr(text))
            m = [e for e in self.choices if e.startswith(text)]
            self.logger.debug('matches = %s', m)
            if len(m) == 1:   # one matching choice
                os.write(ofd, m[0][len(text):])
                return strbuf(m[0])
            elif len(m) == 0:  # no matching choices
                os.write(ofd, '\a') # bell
                return value
            else:  # multiple matching choices
                start = find_longest_common(m)
                self.logger.debug('start = %s', repr(start))
                os.write(ofd, start[len(text):])
                return strbuf(start)
        else:  # no choices
            return value

    def show_tab(self, value):
        self.logger.debug('show_tab(%s)', value)
        ofd = self.stdout.fileno()
        if self.choices:
            os.write(ofd, '\n')
            text = str(value)
            m = [e for e in self.choices if e.startswith(text)]
            self.logger.debug('matches = %s', m)
            numcol = 4
            if m:
                for i, e in enumerate(m):
                    os.write(ofd, '%-20s ' % e)
                    if i != 0 and i % numcol == 0:
                        os.write(ofd, '\n')
                else:
                    if i % numcol != (numcol - 1):
                        os.write(ofd, '\n')
        else:
            self.logger.debug('show_tab: no choices')

    def show_help(self, value):
        self.logger.debug('show_help(%s)', value)
        ofd = self.stdout.fileno()
        if self.helptext:
            os.write(ofd, '\n')
            os.write(ofd, self.helptext)
            os.write(ofd, '\n')

    @staticmethod
    def check_glob(text):
        return '*' in text or '?' in text or ('[' in text and ']' in text)

    def valid_choice(self, text):
        return text in self.choices

    def check_choices(self, text):
        if not self.choices:
            return False
        elif ',' not in text and (
                self.check_glob(text) or self.valid_choice(text)):
            return True
        else:
            for i in text.split(','):
                if not self.check_glob(i) and self.valid_choice(text):
                    return False
            else:
                return True

    def ask(self, allowblank=False):
        ifd = self.stdin.fileno()
        ofd = self.stdout.fileno()
        with rawterminal(ifd):   # change the terminal to raw mode
            while True:
                os.write(ofd, self.make_prompt())
                value = strbuf()
                lasttab = False
                while True:
                    c = os.read(ifd, 1)
                    o = ord(c)
                    self.logger.debug('c = %s; o = %o; value = %s', repr(c), o, value)
                    if lasttab and c != '\t':
                        lasttab = False
                    if c == '':   # EOF
                        raise EOFError
                    elif c == '\x03' or c == '\x04':  # ctrl-C or ctrl-D
                        raise KeyboardInterrupt
                    elif c == '\t' and lasttab:
                        self.show_tab(value)
                        lasttab = False
                        os.write(ofd, self.make_prompt())
                        os.write(ofd, str(value))
                    elif c == '\t':  # tab  completion  ignore for now
                        value = self.handle_tab(value)
                        lasttab = True
                    elif c == ',':  # allow a sequence
                        value.append(c)
                        os.write(ofd, c)
                    elif o == 21:  # ascii (^U) - clear line
                        os.write(ofd, '\b \b' * len(value))
                        del value[:]
                    elif o == 8 or o == 127:  # ascii del (^H) or ^? (bksp)
                        if value:
                            os.write(ofd, '\b \b')  # visual delete
                            del value[-1]
                        else:
                            os.write(ofd, '\a')  # otherwise ring my bell
                    elif o == 31:  # ctrl-/ for help
                        self.show_help(value)
                        os.write(ofd, self.make_prompt())
                        os.write(ofd, str(value))
                    elif c == '\r' or c == '\n':
                        os.write(ofd, '\n')
                        break
                    elif o == 27:  # ascii esc
                        os.write(ofd, '\n')
                        return None
                    elif o < 32:  # ignore other control characters
                        os.write(ofd, '\a')  # ring my bell
                    else:
                        os.write(ofd, c)
                        value.append( c )
                text = str(value)
                if self.default and text == '':
                    text = self.default
                if allowblank and text == '':
                    return text
                elif self.check_choices(text):
                    return text
                elif self.choices:
                    os.write(ofd, 'Not a choice: %s\n' % text)
                elif text:
                    return text
                else:
                    return None

    def askYoN(self):
        ifd = self.stdin.fileno()
        ofd = self.stdout.fileno()
        with rawterminal(ifd):
            os.write(ofd, self.make_prompt())
            while True:
                c = os.read(ifd, 1)
                o = ord(c)
                self.logger.debug('c = %s; o = %o', repr(c), o)
                if c == '':   # EOF
                    raise EOFError
                elif c == '\x03' or c == '\x04':  # ctrl-C or ctrl-D
                    raise KeyboardInterrupt
                elif c in 'Yy':
                    os.write(ofd, c.lower())
                    os.write(ofd, '\n')
                    return True
                elif c in 'Nn':
                    os.write(ofd, c.lower())
                    os.write(ofd, '\n')
                    return False
                elif c == '\r' or c == '\n':
                    os.write(ofd, '\n')
                    if self.default == 'y':
                        return True
                    elif self.default == 'n':
                        return False
                    else:
                        return None
                elif o == 27:  # ascii esc
                    return None
                else:
                    os.write(ofd, '\a')  # ring my bell

@contextlib.contextmanager
def rawterminal(ifd):
    import termios
    old = termios.tcgetattr(ifd)
    new = termios.tcgetattr(ifd)
    new[3] = new[3] & ~termios.ICANON & ~termios.ECHO & ~termios.ISIG
    new[6][termios.VMIN] = 1
    termios.tcsetattr(ifd, termios.TCSADRAIN, new)
    try:
        yield
    finally:
        termios.tcsetattr(ifd, termios.TCSADRAIN, old)

class VersionControl(object):
    prog = None
    dbdirname = None

    def __init__(self, vcdir):
        self.vcdir = vcdir
        if self.dbdirname:
            vcdb = os.path.join(vcdir, self.dbdirname)
            if not os.path.isdir(vcdb):
                raise TypeError('not this type of version control')

    def gencmd(self):
        return ()

    def update(self):
        cmd = self.gencmd()
        if not cmd:
            return
        p = subprocess.Popen(
                cmd,
                cwd=self.vcdir,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
        )
        (sout, serr) = p.communicate()
        if p.returncode != 0:
            raise ValueError(p.returncode, serr)

    @staticmethod
    def new(location):
        for vc in (Mercurial, Subversion, Git, NoVC):
            try:
                c = vc(location)
            except TypeError:
                pass
            else:
                return c


class Git(VersionControl):
    prog = '/usr/bin/git'
    dbdirname = '.git'

    def gencmd(self):
        return ( self.prog, 'pull')

class Mercurial(VersionControl):
    prog = '/usr/bin/hg'
    dbdirname = '.hg'

    def gencmd(self):
        return (self.prog, 'pull', '-u')

class NoVC(VersionControl):
    pass

class Subversion(VersionControl):
    prog = '/usr/bin/svn'
    dbdirname = '.svn'

    def gencmd(self):
        return (self.prog, 'update')

class SvrFilter:
    prog = os.path.join(HomeDir, 'bin', 'svrfilter')
    # taken from Defaults.choices.env in svrfilter.py
    default_envs = ('dev', 'load', 'prod', 'stag', 'test')

    class Entry(object):
        def __init__(self, prod, env, host, user, net, act, apptools):
            self.prod = prod
            self.env = env
            self.host = host
            self.user = user
            self.net = net
            self.active = act
            self.apptools = apptools

    def __call__(self, env, prod, net, user, host, filter):
        cmd = [
            self.prog,
            '--active',
        ]
        if env:
            cmd.extend( ['--env', env] )
        if prod:
            cmd.extend( ['--product', prod] )
        if net:
            cmd.extend( ['--net', net] )
        if user:
            cmd.extend( ['--user', user] )
        if host:
            cmd.extend( ['--host', host] )
        if filter:
            cmd.extend( ['--filter', filter] )
        logging.debug('calling %s', cmd)
        p = subprocess.Popen(
                cmd,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
        )
        (sout, serr) = p.communicate()  # no input
        if p.returncode != 0:
            raise ValueError(p.returncode, serr)
        else:
            logging.debug('svrfilter retrieved %d lines' % sout.count('\n'))
            return sout

    @classmethod
    def possible_envs(cls):
        cmd = [
            cls.prog,
            '--active',
            '--possible', 'env'
        ]
        logging.debug('calling %s', cmd)
        try:
            p = subprocess.Popen(
                cmd,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )
        except:
            return list(cls.default_envs)
        (sout, serr) = p.communicate() # no input
        if p.returncode != 0:
            return list(cls.default_envs)
        else:
            assert sout.startswith('env=')
            field, values = sout.rstrip().split('=', 1)
            return values.split(',')

    @classmethod
    def allvalues(cls, envs):
        cmd = [
            cls.prog,
            '--active',
            '--env',
            ','.join(envs),
            '--raw'
        ]
        logging.debug('calling %s', cmd)
        p = subprocess.Popen(
            cmd,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        (sout, serr) = p.communicate()  # no input
        if p.returncode != 0:
            raise ValueError(p.returncode, serr)
        else:
            fields = [e.split('|') for e in sout.rstrip().split('\n')
                        if e and not e.startswith('#')]
            logging.debug('svrfilter retrieved %d lines' % len(fields))
            return [cls.Entry(*f) for f in fields]



class Butler(object):
    rootdir = os.path.join(HomeDir, 'butler')
    prog = os.path.join(HomeDir, 'bin', 'butler')
    def __init__(self, name):
        assert self.check_datadir(name)
        self.datadir = os.path.normpath(os.path.join(self.rootdir, name))
    def __call__(self, op, input=None, pager=True):
        cmd = [
            self.prog,
            '--directory', self.datadir,
        ]
        if op == None:
            raise ValueError('Missing operation')
        elif op == 'show':
            cmd.append('--show')
        else:
            cmd.extend( ['--op', op] )
        lesscmd = [
            'less',
            '--no-lessopen',
            '--QUIT-AT-EOF',
        ]
        PIPE = subprocess.PIPE
        if pager:
            logging.debug('calling %s', cmd)
            logging.debug('calling %s', lesscmd)
            p = subprocess.Popen(cmd, stdin=PIPE, stdout=PIPE)
            l = subprocess.Popen(lesscmd, stdin=p.stdout, stderr=PIPE,
                    env=dict(os.environ, LESSSECURE='1'),
            )
            p.stdin.write(input)
            p.stdin.close()
            (sout, serr) = l.communicate()
        else:
            logging.debug('calling %s', cmd)
            l = subprocess.Popen(cmd, stdin=PIPE, stderr=PIPE)
            (sout, serr) = l.communicate(input)
        if l.returncode != 0:
            raise ValueError(l.returncode, serr)

    @classmethod
    def check_datadir(cls, name):
        return (
            os.path.isdir(os.path.join(cls.rootdir, name)) and
            os.path.isdir(os.path.join(cls.rootdir, name, 'metadata')) and
            os.path.isdir(os.path.join(cls.rootdir, name, 'templates'))
        )

    @classmethod
    def find_datadirs(cls):
        try:
            return [d for d in sorted(os.listdir(cls.rootdir)) if cls.check_datadir(d)]
        except OSError:
            raise RuntimeError('unable to locate app root directory.')

class App(object):
    config_filename = os.path.join(HomeDir, '.butler-login.ini')
    helptext = {
        'data':
            '''Database to use; each is usually release dependent,
e.g. data-2015.03''',
        'env':
            '''Environment(s) to limit; can be comma-separated
values (not a glob).''',
        'filter':
            '''Filter pattern.''',
        'host':
            '''Hostname(s) to limit; can be a comma-separated
glob pattern.''',
        'net':
            '''Network, e.g. "bos", "ayl; can be a comma-separated
glob pattern."''',
        'op':
            '''Operation to perform:
'diff' is the default and displays the difference between the actual
server(s) configs and what butler would change them to;
'push' performs the actual changes to the server(s) config files;
'show' displays the selected metadata values and templates being included
for each server;
'noop' attempts to run the changes without saving them;
'test' checks if changes would need to be made;
'remote' same as 'push'.
''',
        'preamble':
            '''Butler front-end
Apply changes to a set of servers.
Values are carried over as defaults within the same session.
Each input allows tab-completion of its possible values; can be
comma-separated, glob-denoted values (except for the "data" value).
Press Ctrl-C to exit.
Press Ctrl-/ for help; press ESC to abort an operation.''',
        'prod':
            '''Product(s) to limit; can be a comma-separated
glob pattern.''',
        'user':
            '''Username(s) to limit; can be a comma-separated
glob pattern.''',
    }
    prohibited_envs = ('prod',)

    def __init__(self):
        self.load_config()
        if not os.access(Butler.prog, os.X_OK):
            raise ValueError('cannot find %s' % Butler.prog)
        if not os.access(SvrFilter.prog, os.X_OK):
            raise ValueError('cannot find %s' % SvrFilter.prog)
        self.datadir_names = Butler.find_datadirs()
        if len(self.datadir_names) == 0:
            raise ValueError('No data directories found.')
        # cache all the values so we can get limit choices
        self.allowable_envs = SvrFilter.possible_envs()
        for env in self.prohibited_envs:
            try:
                self.allowable_envs.remove(env)
            except ValueError:
                pass
        self.values = SvrFilter.allvalues(envs=self.allowable_envs)

    def load_config(self):
        if os.path.isfile(self.config_filename):
            for line in [l.decode('latin-1').rstrip() for l in open(self.config_filename)]:
                if line and not line.startswith('#'):
                    var, val = line.split('=')
                    var = var.strip()
                    if var == 'rootdir':
                        Butler.rootdir = val.rstrip()
                    elif var == 'program.butler':
                        Butler.prog = os.path.expanduser(val.strip())
                    elif var == 'program.svrfilter':
                        SvrFilter.prog = os.path.expanduser(val.strip())
                    elif var == 'logging.root':
                        try:
                            level = getattr(logging, val.upper())
                        except:
                            pass
                        else:
                            logging.getLogger('').level = level
                    elif var == 'logging.prompt':
                        try:
                            level = getattr(logging, val.upper())
                        except:
                            pass
                        else:
                            logging.getLogger('prompt').level = level
                    elif var == 'env.prohibited':
                        self.prohibited_envs = val.replace(',', ' ').split()

    def getvalues(self, field, env=None, net=None, prod=None, user=None, host=None):
        logging.debug('limiters: env=%s, net=%s, prod=%s, user=%s, host=%s', env, net, prod, user, host)
        values = set(
            [getattr(e, field) for e in self.values
                if (not env or e.env == env) and
                   (not net or e.net == net) and
                   (not prod or e.prod == prod) and
                   (not user or e.user == user) and
                   (not host or e.host == host)
            ]
        )
        logging.debug('values = %s', values)
        return sorted(values)

    def write(self, msg='', nonewline=False):
        if msg:
            os.write(sys.stdout.fileno(), msg)
        if not nonewline:
            os.write(sys.stdout.fileno(), '\n')

    def run(self):
        opcmds = ['show', 'push', 'remote', 'diff', 'noop', 'test']
        net = prod = user = host = filter = None
        env = 'test'
        op = 'diff'
        self.write(self.helptext['preamble'])
        self.write()
        data = None
        while True:
            try:
                data = Prompter('Data',
                                data or self.datadir_names[-1],
                                self.datadir_names,
                                help=self.helptext['data']
                ).ask()
                if data is not None:
                    try:
                        butler = Butler(data)
                    except AssertionError:
                        continue
                    svrfilter = SvrFilter()
                    try:
                        VersionControl.new(butler.datadir).update()
                    except ValueError:
                        logging.error('unable to update version control')
                    env = Prompter('Env', env,
                        self.getvalues('env'),
                        help=self.helptext['env']
                    ).ask()
                    net = Prompter('Net', net,
                        self.getvalues('net', env=env),
                        help=self.helptext['net']
                    ).ask(True)
                    prod = Prompter('Product', prod,
                        self.getvalues('prod', env=env, net=net),
                        help=self.helptext['prod']
                    ).ask(True)
                    user = Prompter('User', user,
                        self.getvalues('user', env=env, net=net, prod=prod),
                        help=self.helptext['user']
                    ).ask(True)
                    host = Prompter('Host', host,
                        self.getvalues('host', env=env, net=net, prod=prod, user=user),
                        help=self.helptext['host']
                    ).ask(True)
                    filter = Prompter('Filter', filter,
                        help=self.helptext['filter']
                    ).ask(True)
                    op = Prompter('Operation', op, opcmds,
                        help=self.helptext['op']
                    ).ask()
                    if op is not None:
                        try:
                            entries = svrfilter(env, prod, net, user, host, filter)
                        except ValueError, e:
                            if len(e.args) > 1:
                                self.write('!%s' % e[1])
                            else:
                                self.write('!%s' % e)
                        else:
                            try:
                                butler(op, entries, pager=False)
                            except ValueError, e:
                                if len(e.args) == 1 and isinstance(e.args[0], str):
                                    self.write('!%s' % e)
                                elif len(e.args) == 1:
                                    self.write('![%d] Error' % e.args[0])
                                elif isinstance(e.args[0], int):
                                    self.write('![%d] %s\n' % e.args[:2])
                                else:
                                    self.write('!%s' % e)
            except KeyboardInterrupt:
                self.write()
            if not Prompter('Again?', 'y').askYoN():
                break

def set_logging():
    logdir = os.path.join(HomeDir, 'logs')
    if not os.path.isdir(logdir):
        os.mkdir(logdir)
    logging.basicConfig(
        filename=os.path.join(logdir, 'butler-frontend.log'),
        level=logging.DEBUG
    )
    logging.getLogger('prompt').level = logging.WARN

if __name__ == '__main__':
    set_logging()
    try:
        app = App()
    except Exception, e:
        raise SystemExit(e)
    try:
        app.run()
    except (EOFError, KeyboardInterrupt):
        raise SystemExit
    except ValueError, e:
        try:
            if e.args[1]:
                raise SystemExit(e.args[1])
        except IndexError:
            raise SystemExit(e.args[0])
