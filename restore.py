#!/usr/bin/python
# restore - Michael P. Reilly Copyright 2014-2016 All rights reserved

import getopt
import os
import subprocess
import sys

homedir = os.path.expanduser('~')
backupdir = os.path.join(homedir, 'backups')


def do_help():
    print sys.argv[0], '[options] [extracts]'
    print '-h | --help              this output'
    print '-f file | --file=file    specific backup file to use'
    print '-l | --list              list backup files'
    print '-n | --newest            use newest instead of oldest backup'
    print '-N | --noop              show what would happen'
    print '-v | --verbose           show more output'


def main(filename, args, listfiles, newest, noop, verbose):
    butler_backups = []
    if filename is not None:
        butler_backups = [os.path.normpath(filename)]
        if not os.path.exists(butler_backups[0]):
            raise SystemExit('No such file or directory: %s' % filename)
    else:
        for fname in os.listdir(backupdir):
            if fname.startswith('butler.'):
                butler_backups.append(os.path.join(backupdir, fname))
    butler_backups.sort()
    if newest:
        butler_backups.reverse()

    if listfiles:
        for fname in butler_backups:
            print fname
    else:
        c = noop and 'tz' or 'xz'
        if verbose:
            c += 'v'
        c += 'fC'
        try:
            tarfile = butler_backups[0]
        except IndexError:
            print 'Cannot find butler backups'
        else:
            cmd = ('tar', c, tarfile, homedir) + tuple(args)
            if debug:
                print cmd
            rc = subprocess.call(cmd)
            if rc:
                raise SystemExit('Tar error: %d' % rc)
            elif not verbose:
                print 'Files restored.'

shopts = 'f:hlnNv'
lgopts = ('file=', 'help', 'list', 'newest', 'noop', 'verbose')
debug = 'DEBUG' in os.environ and os.environ['DEBUG']

if __name__ == "__main__":
    backupfile = None
    listfiles = newest = noop = verbose = False
    # noop = True
    try:
        opts, args = getopt.getopt(sys.argv[1:], shopts, lgopts)
    except getopt.error, e:
        raise SystemExit(e)
    else:
        for o, v in opts:
            if o == '--':
                break
            elif o in ('-h', '--help'):
                do_help()
                raise SystemExit
            elif o in ('-f', '--file'):
                backupfile = v
            elif o in ('-l', '--list'):
                listfiles = True
            elif o in ('-n', '--newest'):
                newest = True
            elif o in ('-N', '--noop'):
                noop = True
            elif o in ('-v', '--verbose'):
                verbose = True

    main(backupfile, args, listfiles, newest, noop, verbose)
