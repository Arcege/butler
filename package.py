#!/usr/bin/python -u
# package - Michael P. Reilly Copyright 2014-2016 All rights reserved
# Options:
# --help|-h               options and the like
# --version|-V            shows version information
# --noop|-N               do not perform operations (implies --verbose)
# --quiet|-q              fewer messages displayed
# --verbose|-v            more messages displayed
# --directory=DIR|-d DIR  install root directory (default is $HOME)
# --diff|-D               show differences (implies --noop)
# --info                  show files included (with --verbose)
# --whitespace|-W         ignore whitespace, with --diff

# this program assumes to be running on much older versions of Python 2
# where functionality may not have existed yet
# this program is _not_ Python 3 compatible, nor should it be

from __future__ import generators

import errno
try:
    from hashlib import md5
except ImportError:
    from md5 import md5
from fnmatch import fnmatch
import getopt
import os
import shutil
import sys
import time

# The next section is to simulate functionality that is standard in later
# releases of Python, but not available in earlier releases.
# function os.walk
# module logging
# module tarfile

try:
    import logging
    import logging.handlers
except ImportError:
    # if the logging modules does not exist (older Python), then simulate
    class Logging(object):
        stream = sys.stdout
        DEBUG = 0
        INFO = 5
        WARNING = 10
        ERROR = 15
        CRITICAL = 15
        file = None
        level = ERROR  # default

        def setfile(self):
            if not os.path.isdir(App.logdir):
                mkdir(App.logdir)
            filename = os.path.join(App.logdir, 'butler.log')
            self.file = open(filename, 'w+')

        def setLevel(self, level):
            self.level = level

        def message(self, etype, msg, *args, **kwargs):
            logmsg = msg % args
            if etype >= self.level:
                if self.file:
                    self.file.write(logmsg)
                    self.file.write(os.linesep)
                    self.file.flush()
                self.stream.write(logmsg)
                self.stream.write(os.linesep)
                self.stream.flush()

        def critical(self, msg, *args, **kwargs):
            self.message(self.CRITICAL, msg, *args, **kwargs)
        fatal = critical

        def error(self, msg, *args, **kwargs):
            self.message(self.ERROR, msg, *args, **kwargs)

        def warning(self, msg, *args, **kwargs):
            self.message(self.WARNING, msg, *args, **kwargs)
        warn = warning

        def info(self, msg, *args, **kwargs):
            self.message(self.INFO, msg, *args, **kwargs)

        def debug(self, msg, *args, **kwargs):
            self.message(self.DEBUG, msg, *args, **kwargs)
        exception = error
    logging = Logging()
else:
    Logging = None

try:
    import tarfile
except ImportError:
    # if the tarfile module does not exist (older Python), then simulate
    import tempfile

    class tarfile(object):
        tmpdir = None
        _cmdcache = {'rmtree': shutil.rmtree}

        def __init__(self, filename, mode='w:gz'):
            self.filename = filename
            basename = os.path.splitext(self.filename)[0]
            self.tmpfile = basename + '.tar'
            self.mode = mode

        def open(cls, filename, mode='w:gz'):
            return cls(filename, mode=mode)
        open = classmethod(open)

        def add(self, name, arcname=None):
            if name == arcname:
                cmnname = os.curdir
            elif arcname is None:
                cmnname, arcname = os.curdir, name
            else:
                cmnname = os.path.commonprefix((name, arcname))
                if cmnname == '' and len(name) > len(arcname):
                    cmnname = name[:-len(arcname)]
            cmnname = os.path.expanduser(os.path.normpath(cmnname))
            cmd = 'tar ufC %s %s %s' % (self.tmpfile, cmnname, arcname)
            p = os.popen(cmd, 'r')
            p.close()

        def close(self):
            cmd = 'gzip %s' % self.tmpfile
            p = os.popen(cmd, 'r')
            p.close()
            os.rename('%s.gz' % self.tmpfile, self.filename)

try:
    enumerate
except NameError:
    def enumerate(seq):
        for i in range(len(seq)):
            yield (i, seq[i])

ReleaseNo = '$ReleaseNo$'
_revision_num = '$Revision$'

# information from Butler per instantiation
GENERATED_WHEN = '''${GENERATED_WHEN}'''
GENERATED_FOR = '''${GENERATED_FOR}'''
DB_VERSION = '''${DB_VERSION}'''
CHANGE_REQUEST = '''${CHANGE_REQ}'''
IS_CR_REQUIRED = '''${IS_CR_REQ}'''

# Data structures from Butler
# Filelist: ${FILELIST}
# Allows: ${ALLOWS}
# Ignores: ${IGNORES}
# Excludes: ${EXCLUSIONS}
# Dynamics: ${DYNAMICS}

homedir = os.path.expanduser('~')

# global variables to hold logger instances
appdir = os.path.join(homedir, '.butler')
logdir = appdir

# for older Python
# no sorted, so simulate
try:
    sorted
except NameError:
    def sorted(sequence):
        if isinstance(sequence, dict):
            s = sequence.keys()
        else:
            s = list(sequence[:])  # list doesn't duplicate, but do force
        s.sort()
        return s
# no key= or reverse= keywords, so simulate
try:
    [].sort(key=None)
except TypeError:
    def _sort(seq, cmpfunc=None, key=None, reverse=False):
        if cmpfunc is None and key is None:
            seq.sort()
        elif key is None:
            seq.sort(cmpfunc)
        elif cmpfunc is None:
            seq.sort(lambda a, b: cmp(key(a), key(b)))
        else:
            seq.sort(lambda a, b: cmpfunc(key(a), key(b)))
        if reverse:
            seq.reverse()
else:
    def _sort(seq, cmpfunc=None, key=None, reverse=False):
        seq.sort(cmpfunc, key=key, reverse=reverse)


# handleOSError(exception) -> str
def handleOSError(e):
    '''Return the string for comment errors.'''
    if e.errno == errno.ENOMEM:
        return 'Not enough memory'
    elif e.errno == errno.ENOSPC:
        return 'No disk space'
    elif e.errno == errno.EROFS:
        return 'Read-only filesystem'
    else:
        return str(e)


# mkdir(str) -> bool
def mkdir(dir):
    '''Recursive mkdir.'''
    rc = False
    if dir != os.sep and dir != '':
        par = os.path.dirname(dir)
        if not os.path.exists(par):
            rc = mkdir(par)
        try:
            if not os.path.exists(dir):
                os.mkdir(dir)
                rc = True
        except OSError:
            t, e, tb = sys.exc_info()
            raise t, '%s: %s' % (dir, e), tb
    return rc


# Packet() -> None
# mostly a helper to automatically decode a specific set of data
class Packet(object):
    '''An encapsulation of the data from Butler, automatically
decoded on instantiation.'''
    attrnames = ['data', 'allows', 'ignores', 'excludes', 'dynamics']

    def __init__(self):
        for attr in self.attrnames:
            value = getattr(self.__class__, attr.upper())
            setattr(self, attr, self.decode(value))

    # decode(str) -> str
    # @staticmethod (use old style to support Py4.3 and earlier)
    def decode(data):
        '''Decode the data generated by butler.  The current implementation
should work on Python 2.0 or later.  If there is no b64decode, then use
binascii and a loop.'''
        from zlib import decompress
        from pickle import loads
        try:
            from base64 import b64decode
        except:
            from binascii import a2b_base64
            accum = []
            for line in data.rstrip().split('\n'):
                accum.append(a2b_base64(line))
            bdata = ''.join(accum)
        else:
            bdata = b64decode(data)
        return loads(decompress(bdata))
    decode = staticmethod(decode)


# toplevel(str) -> str
def toplevel(path):
    '''Find the top level component.
toplevel("foobar") => "foobar"
toplevel("/foobar") => "foobar"
toplevel("foobar/xyzzy/widget") = "foobar"
'''
    d = os.path.dirname(path)
    if d in ('', os.sep):
        return path
    return toplevel(d)



# samepath(str, str) -> bool
# check if two paths are in a common structure
def samepath(filename1, filename2):
    fn1 = os.path.realpath(filename1)
    fn2 = os.path.realpath(filename2)
    return os.path.commonpath((fn1, fn2)) not in ('', os.curdir, os.sep)

# find(str, str, seq, seq, seq) -> generator
def find(top, tdir, allow=(), ignore=(), excludes=()):
    '''A generator to traverse a directory tree, ignoring certain directories.
'''
    # this makes sure that c will end with a '/', used to remove the leading
    # string of a pathname
    c = os.path.commonprefix([os.path.join(top, f) for f in ('a', 'b')])

    def check(fn, allows=allow, ignores=ignore):
        for a in allows:
            if os.path.commonprefix((fn, a)) in (fn, a):
                return True
        for i in ignores:
            if os.path.commonprefix((fn, i)) in (fn, i):
                return False
        return True
    try:
        from os import walk
    except ImportError:

        # on older versions without os.walk, need to use os.path.walk
        def walker(arg, dirname, names):
            torem = []
            for name in names:
                f = os.path.join(dirname, name)
                if os.path.isdir(f) and not check(f):
                    logging.debug('ignoring component %s', f)
                    torem.append(name)
                else:
                    for patt in excludes:
                        if fnmatch(f, patt):
                            logging.debug(
                                'ignoring %s because of %s', f, patt
                            )
                            break
                    else:
                        yield f
            # prune anything we don't want to look at
            for name in torem:
                names.remove(name)
        os.path.walk(os.path.join(top, tdir), walker, None)
    else:
        for dirpath, dirnames, filenames in walk(os.path.join(top, tdir)):
            d = dirpath.replace(c, '')
            torem = []
            for fn in dirnames:
                f = os.path.join(d, fn)
                if not check(f):
                    logging.debug('ignoring component %s', f)
                    torem.append(fn)
            for fn in torem:
                dirnames.remove(fn)
            for fn in filenames:
                f = os.path.join(d, fn)
                # anything that matches is excluded
                for patt in excludes:
                    if fnmatch(f, patt):
                        logging.debug('ignoring %s because of %s', f, patt)
                        break
                else:
                    yield f


class File(object):
    dtype = 'file'
    patchdir = os.path.join(appdir, 'preserve')

    def __init__(self, key, pack, rootdir, data):
        self.key = key
        self.pack = pack
        self.root = rootdir
        self.filename = os.path.join(rootdir, key)
        self.ftype = self.type()
        self.data = self.adjust_data(data)
        if self.data:
            newmd5 = md5(self.data)
        else:
            newmd5 = md5()
        olddata = None
        oldmd5 = md5()
        self.broken = self.isbroken()
        try:
            if self.ftype == 'nofile':  # new file being added
                self.op = 'add'
            elif self.dtype == 'nofile':  # file being removed
                self.op = 'remove'
            elif self.dtype != self.ftype:
                self.op = 'conflict'
            elif self.ftype == 'file':
                self.op = 'change'
                oldmd5.update(open(self.filename, 'rt').read())
            elif self.ftype == 'symlink':
                self.op = 'change'
                oldmd5.update(os.readlink(self.filename))
            else:
                logging.error('Bad type (%s) for %s', data[key][0], key)
                raise ValueError('invalid file type')
        except (IOError, OSError):
            self.op = 'change'
        if self.op == 'change':
            if newmd5.digest() == oldmd5.digest():
                self.op = 'ignore'
            elif self.isdynamic():
                self.op = 'patch'
            else:
                self.op = 'update'
        assert self.op in ('add', 'conflict', 'ignore', 'patch', 'remove',
                'update')
        self.tobackup = (
            self.op in ('conflict', 'patch', 'remove', 'update'))
        logging.debug('%s op=%s; backup=%s', self.key, self.op, self.tobackup)

    def adjust_data(self, data):
        return data

    def __repr__(self):
        return '<%s %s %s>' % (self.dtype, self.key, self.op)
    def __str__(self):
        return self.key
    def __len__(self):
        return len(self.key)
    def __lt__(self, other):
        return self.key < other
    def __eq__(self, other):
        return self.key == other
    def __hash__(self):
        return hash(self.key)

    # type(str)  - return a symbolic string of the file type
    def type(self):
        if os.path.islink(self.filename):
            return 'symlink'
        elif os.path.isdir(self.filename):
            return 'dir'
        elif os.path.isfile(self.filename):
            return 'file'
        elif not os.path.exists(self.filename):
            return 'nofile'
        else:
            return 'other'

    def isbroken(self):
        '''Does the path on the fs have a symlink and we are trying to place
something lower?'''
        fname = os.path.dirname(self.filename)
        root = os.path.normpath(self.root)

        while True:
            if fname == root or fname == os.sep:
                return None
            elif os.path.islink(fname):
                return fname
            fname = os.path.dirname(fname)

    def isdynamic(self):
        '''Does the path match one of the glob patterns in the list?'''
        for patt in self.pack.dynamics:
            if fnmatch(self.key, patt):
                # logging.debug('match %s against %s' % (path, patt))
                return True
        else:
            return False

    def dump(self):
        mkdir(os.path.dirname(self.filename))
        open(self.filename, 'wb').write(self.data)

    def diff(self, whitespace):
        if whitespace:
            diffopt = '-uw'
        else:
            diffopt = '-u'
        if self.op == 'add':
            sys.stdout.write('new file %s\n' % self.key)
        elif self.ftype == 'dir':
            when = mtime(self.filename)
            msg = '--- %s (dir) %s\n+++ (file)\n' % (self.filename, when)
            sys.stdout.write(msg)
        elif self.op == 'patch':
            pass
        elif self.op == 'update':
            cmd = 'diff %s "%s" -' % (diffopt, self.filename)
            p = os.popen(cmd, 'w')
            logging.debug('cmd = %s', cmd)
            try:
                p.write(self.data)
            except IOError:
                logging.warn('Cound not diff %s', self.filename)
            p.close()
        elif self.op == 'conflict':
            pass

    def perform(self, noop):
        logging.info('%s %s', self.op, self.key)
        if noop:
            return
        mkdir(os.path.dirname(self.filename))
        if self.op in ('add', 'update'):
            open(self.filename, 'wt').write(self.data)
            logging.info('updated %s', self.filename)
        elif self.op == 'patch':
            import tempfile
            fname = self.filename
            pname = os.path.join(self.patchdir, self.key)
            tname = tempfile.mktemp(dir=self.patchdir)
            logging.debug('fname=%s\npname = %s\ntname = %s', fname, pname, tname)
            if os.path.exists(pname) and os.path.exists(fname):
                # retrieve preserved data
                if os.path.exists(fname):
                    fdata = open(fname, 'rt').read()
                else:
                    fdata = ''
                pdata = open(pname, 'rt').read()
                if self.data == pdata:
                    logging.info('ignoring %s', self.key)
                # no changes since last run, so no need to patch
                elif fdata == pdata:
                    open(fname, 'wt').write(self.data)
                    logging.debug('updated %s', fname)
                    open(pname, 'wt').write(self.data)
                    logging.debug('preserved %s', pname)
                else:
                    open(tname, 'wt').write(self.data)  # save for diff3
                    try:
                        # the order is important to get the correct flow
                        cmd = 'diff3 -e "%s" "%s" "%s"' % (pname, fname, tname)
                        f = os.popen(cmd, 'r')
                        mdata = f.read()  # an ed(1) script, without the 'w' cmd
                        rc = f.close()
                        if rc == 0 or rc is None:
                            f = os.popen('ed - "%s"' % fname, 'w')
                            f.write(mdata)
                            f.write('w\n')
                            rc = f.close()
                            logging.debug('patched %s', fname)
                            if rc == 0 or rc is None:
                                mkdir(os.path.dirname(pname))
                                open(pname, 'wt').write(self.data)
                                logging.debug('preserved %s', pname)
                            else:
                                raise IOError(
                                    'no patch generated for %s [rc=%d]' %
                                              (self.key, rc))
                        else:
                            raise IOError('no patch generated for %s (rc=%d)' %
                                          (key, rc))
                    finally:
                        os.remove(tname)
            # either no file or no preserved file
            else:
                open(fname, 'wt').write(self.data)
                logging.debug('updated %s', fname)
                mkdir(os.path.dirname(pname))
                open(pname, 'wt').write(self.data)
                logging.debug('preserved %s', pname)
        elif self.op == 'conflict':
            if self.ftype == 'symlink':
                os.remove(self.filename)
            elif self.ftype == 'dir':
                from shutil import rmtree
                rmtree(self.filename)
            open(self.filename, 'wt').write(self.data)
            logging.debug('%s conflict; updated %s', self.ftype, self.filename)
            if self.isdynamic():
                pname = os.path.join(self.patchdir, self.key)
                mkdir(os.path.dirname(pname))
                if self.ftype == 'symlink':
                    os.remove(pname)
                elif self.ftype == 'dir':
                    from shutil import rmtree
                    rmtree(pname)
                open(pname, 'wt').write(self.data)
                logging.debug('preserved %s', pname)


class Symlink(File):
    dtype = 'symlink'

    def adjust_data(self, data):
        from os.path import expandvars, expanduser
        return expandvars(expanduser(data))

    def dump(self):
        mkdir(os.path.dirname(self.filename))
        os.symlink(self.data, self.filename)

    def diff(self, whitespace):
        if self.op == 'add':
            msg = '---\n+++ %s\n@@ -1 +1 @@\n+@%s\n'
            sys.stdout.write(msg % (self.key, self.data))
        elif self.op in ('patch', 'update'):
            x = os.readlink(self.filename)
            if x != self.data:
                when = mtime(self.filename)
                msg = '--- %s %s\n+++ -\n@@ -1 +1 @@\n-@%s\n+@%s\n'
                sys.stdout.write(msg % (self.key, when, x, self.data))
        elif self.op == 'conflict':
            when = mtime(self.filename)
            msg = '--- %s (%s) %s\n+++ (symlink)\n'
            sys.stdout.write(msg % (self.key, self.ftype, when))

    def perform(self, noop):
        logging.info('%s %s', self.op, self.key)
        if noop:
            return
        if self.op in ('add', 'patch', 'update'):
            if os.path.islink(self.filename):
                os.remove(self.filename)
                logging.debug('removed old %s', self.filename)
            mkdir(os.path.dirname(self.filename))
            os.symlink(self.data, self.filename)
            logging.debug('symlink %s', self.filename)
        elif self.op == 'conflict':
            if os.path.exists(self.filename):
                if self.ftype == 'file':
                    os.remove(self.filename)
                elif self.ftype == 'dir':
                    from shutil import rmtree
                    rmtree(self.filename)
            os.symlink(self.data, self.filename)
            logging.debug('%s conflict resolved; symlink %s', self.op, self.filename)


class Broken(File):
    dtype = 'broken'
    op = 'remove'

    def __init__(self, key, pack, rootdir, data):
        self.key = key.replace(rootdir + os.sep, '')
        self.pack = pack
        self.filename = key
        self.ftype = 'symlink'
        self.data = None
        self.tobackup = True

    def dump(self):
        pass
    def diff(self, whitespace):
        pass
    def perform(self, noop):
        logging.info('occluding %s', self.key)
        if noop:
            return
        os.remove(self.filename)
        logging.debug('removed occluding symlink %s', self.filename)


class NoFile(File):
    dtype = 'nofile'

    def dump(self):
        pass
    def diff(self, whitespace):
        if self.ftype == 'symlink':
            x = os.readlink(self.filename)
            msg = '--- %s\n+++\n@@ -1 @@\n-@%s\n'
            sys.stdout.write(msg % (self.key, x))
        elif self.ftype == 'file':
            sys.stdout.write('remove file %s\n' % self.key)

    def perform(self, noop):
        if noop:
            return
        logging.info('%s %s', self.op, self.key)
        if self.ftype == 'dir':
            from shutil import rmtree
            rmtree(self.filename)
            logging.debug('removed %s', self.filename)
        elif self.ftype in ('file', 'symlink'):
            os.remove(self.filename)
            logging.debug('removed %s', self.filename)
        else:
            assert self.ftype


def Entry(key, pack, rootdir):
    try:
        dtype = pack.data[key][0]
        data = pack.data[key][1]
    except KeyError:
        return NoFile(key, pack, rootdir, None)
    else:
        if dtype == 'file':
            return File(key, pack, rootdir, data)
        elif dtype == 'symlink':
            return Symlink(key, pack, rootdir, data)


def mtime(filename):
    s = os.lstat(filename)
    return time.ctime(s[os.path.stat.ST_MTIME])


# Backup(dir)
class Backup(object):
    def __init__(self, homedir, rootdir):
        self.dir = os.path.join(homedir, 'backups')
        self.root = rootdir
        self.files = []
        self.name = time.strftime('butler.%Y%m%d-%H%M%S.tgz')
        tfname = os.path.join(self.dir, self.name)
        mkdir(self.dir)
    def close(self):
        if self.files:
            tfname = os.path.join(self.dir, self.name)
            tfile = tarfile.open(tfname, mode='w:gz')
            for fn in self.files:
                tfile.add(name=os.path.join(self.root, fn), arcname=fn)
            tfile.close()
            logging.warn('backup at %s', self.name)
    def add(self, filename):
        self.files.append(filename)
        logging.debug('backing up %s', filename)
    def clean_old(self, age=90): # days
        '''Remove backups/butler.*.tgz files older than age days.'''
        from glob import glob
        when = time.time() - (3600 * 24 * age) # age days ago
        patt = os.path.join(self.dir, 'butler.*.tgz')
        for fn in glob(patt):
            if os.path.getmtime(fn) < when:  # older/earlier
                try:
                    os.remove(fn)
                except OSError:
                    logging.warn('cannot remove %s', fn)
                else:
                    logging.info('removing %s', fn)


# App()
# the main routine
class App(object):
    homedir = os.path.expanduser('~')
    logdir = appdir

    class Error(Exception):
        '''Notify the calling context (__main__) to exit
abnormally.'''

    class Exit(Exception):
        '''Notify the calling context (__main__) to exit
normally.'''

    def __init__(self):
        try:
            self.opts = opts = Opts()
        except SystemExit:
            raise App.Exit
        except getopt.error, e:
            logging.error(e)
            raise App.Error
        else:
            if opts.args:  # no command-line arguments, just options
                logging.error('unexpected command-line arguments')
                return 1

        self.setuplogging()

        # we want to be sure that we're dumping files to someplace new,
        # to ensure that we wouldn't be overwriting anything
        if opts.dump and os.path.exists(self.opts.rootdir):
            logging.error('Error: --dump requires non-existent directory')
            raise App.Exit
        # otherwise we need to change against (possibly) pre-existing files
        elif not opts.dump and not os.path.isdir(self.opts.rootdir):
            logging.error('expecting directory: %s', self.opts.rootdir)
            raise App.Exit

        # this will raise an exception if the data is invalid
        # do this after setuplogging
        try:
            self.pack = Pack()
        except TypeError:
            logging.critical('Butler data is invalid')
            raise App.Exit

    # setuplogging() -> None
    def setuplogging(self):
        '''Set up the logging to stdout showing just the message.
If we will do anything (not noop), then also open the butler.log
file and send timestamped messages there.'''
        # if noop, then do not emit messages to butler.log
        if Logging is None:
            logger = logging.getLogger()  # root logger
            logger.setLevel(logging.DEBUG)
            ch = logging.StreamHandler(sys.stdout)
            ch.setLevel(self.opts.loglevel)
            ch.setFormatter(logging.Formatter(fmt='%(message)s'))
            logger.addHandler(ch)
            if not self.opts.noop:
                try:
                    if not os.path.isdir(self.logdir):
                        mkdir(self.logdir)
                    filename = os.path.join(self.logdir, 'butler.log')
                    try:
                        fh = logging.handlers.RotatingFileHandler(
                            filename, maxBytes=1048576, backupCount=9)
                    except NameError:
                        fh = logging.FileHandler(filename)
                    fh.setFormatter(
                        logging.Formatter(
                            fmt='%(asctime)s %(levelname)s %(message)s')
                    )
                    fh.setLevel(logging.INFO)
                except IOError:
                    logging.warning('Could not create butler.log')
                else:
                    logger.addHandler(fh)
        # if no standard logger library
        else:
            logging.setLevel(self.opts.loglevel)
            if not self.opts.noop:
                logging.setfile()

    # run() -> bool
    # exceptions: App.Error, App.Exit
    def run(self):
        '''Either output the package info or do some real work.'''
        if self.opts.doinfo:
            self.show_info()
        else:
            self.operate()

    # show_tag(str, str, indent=bool) -> None
    # @staticmethod (use old style to support Py2.3 and earlier)
    def show_tag(tag, seq, indent=False):
        '''If not empty, output the tag and the sequence.'''
        if seq:
            print tag
            for key in seq:
                if indent:
                    print '\t',
                print key
    show_tag = staticmethod(show_tag)

    # show_info() -> None
    def show_info(self):
        '''Only output the package info and exit.'''
        self.show_tag('Filelist:', sorted(self.pack.data),
                      self.opts.loglevel == logging.INFO)
        if self.opts.loglevel == logging.INFO:  # verbose
            self.show_tag('Allows:', self.pack.allows, indent=True)
            self.show_tag('Ignores:', self.pack.ignores, indent=True)
            self.show_tag('Excludes:', self.pack.excludes, indent=True)
            self.show_tag('Dynamics:', self.pack.dynamics, indent=True)

    # operate() -> None
    # exceptions: App.Error, App.Exit
    def operate(self):
        '''The main algorithm, determine which files to be changed/patched/
removed, create a backup, output description to log, perform operations,
including pruning any empty directories in the roots.'''
        pack = self.pack
        opts = self.opts
        entries = []
        roots = []

        self.check_data(entries, roots)

        # broken are symlinks that would be removed by a file being
        # added/updated; this is a special case of a conflict
        for fn in set([e.broken for e in entries if e.broken]):
            entries.append(Broken(fn, None, opts.rootdir, None))

        # find files that should not be there and mark for deletion
        self.find_files_to_delete(opts.rootdir, entries, roots)

        # boolean if there are any changes expected
        changes = [e for e in entries
                    if e.op in ('conflict', 'patch', 'remove', 'update', 'add')]
        tobackup = [e.key for e in entries if e.tobackup]

        # for --test, we only check if things would be done
        if opts.testonly:
            if changes:
                raise App.Error
            else:
                raise App.Exit
        elif not changes:
            logging.info('nothing to update')
        else:
            # if we are not dumping or showing differences, and a CR
            # is required but not given, then fail
            if (not opts.showdiff and not opts.dump and
                IS_CR_REQUIRED == "True" and not CHANGE_REQUEST):
                logging.error('CR is required')
                raise App.Error

            logging.info('butler/package.py %s (r%s)', ReleaseNo,
                         _revision_num)
            logging.info('generated for %s', GENERATED_FOR)
            logging.info('on %s', GENERATED_WHEN)
            logging.info('from %s', DB_VERSION)
            if CHANGE_REQUEST:
                logging.info('CR %s', CHANGE_REQUEST)
            else:
                logging.info('no CR given')
            logging.info('rooted at %s', opts.rootdir)

            logging.debug('roots = %s', roots)
            logging.debug('conflicts = %s',
                    [e for e in entries if e.op == 'conflict'])
            logging.debug('patches = %s',
                    [e for e in entries if e.op == 'patch'])
            logging.debug('updates = %s',
                    [e for e in entries if e.op == 'update'])
            logging.debug('additions = %s',
                    [e for e in entries if e.op == 'add'])
            logging.debug('removals = %s',
                    [e for e in entries if e.op == 'remove'])
            logging.debug('tobackup = %s',
                    [e for e in entries if e.tobackup])

            # at this point, if not noop, we'll start making changes
            try:
                if not opts.noop and tobackup:
                    backup = Backup(homedir, opts.rootdir)
                    for e in tobackup:
                        backup.add(e)
                    backup.clean_old()
                    backup.close()
                elif not tobackup:
                    logging.info('nothing to be backed up')

                # do removals before anything else
                dirstocheck = []
                removals = [e for e in entries if e.op == 'remove']
                _sort(removals, key=len, reverse=False)
                for entry in removals:
                    if opts.showdiff:
                        entry.diff(opts.diffwhitespace)
                    elif opts.dump:
                        entry.dump()
                    else:
                        entry.perform(opts.noop)
                        if entry.op == 'remove' and not opts.noop:
                            dirstocheck.append(os.path.dirname(entry.key))
                for entry in entries:
                    if entry in removals:
                        continue
                    logging.debug('doing %s', entry)
                    if opts.showdiff:
                        entry.diff(opts.diffwhitespace)
                    elif opts.dump:
                        entry.dump()
                    else:
                        entry.perform(opts.noop)

                # prune now empty directories we may have touched
                _sort(dirstocheck, key=len, reverse=True)
                for name in dirstocheck:
                    dirname = os.path.join(opts.rootdir, name)
                    if os.path.isdir(dirname) and \
                            len(os.listdir(dirname)) == 0: # empty directory
                        try:
                            os.rmdir(dirname)
                        except OSError:
                            logging.error('could not remove %s', name)
                        else:
                            logging.info('removed %s', name)
                            dirstocheck.append(os.path.dirname(name))

            except (IOError, OSError), e:
                logging.error(handleOSError(e))
                raise App.Error

            if not opts.noop:
                logging.info('completed')

    # find_files_to_delete(str, Packet, list, Removals, list) -> None
    def find_files_to_delete(self, rootdir, entries, roots):
        '''Find files in the root directories that are not present in the
packed data.  Add them to the files to backup as well.'''
        logging.debug('find removals')
        for root in roots:
            for fname in find(rootdir, root,
                              allow=self.pack.allows,
                              ignore=self.pack.ignores,
                              excludes=self.pack.excludes):
                if fname not in self.pack.data:
                    if fname not in entries:
                        f = Entry(fname, self.pack, rootdir)
                        entries.append(f)

    # check_data(Updates, Patches, Removals, list, list) -> None
    # exceptions: App.Error
    def check_data(self, entries, roots):
        '''Traverse the packed data, identifying which files would need to be
updated, patched, removed and backed up.'''
        error = False
        data = self.pack.data
        logging.debug('checking files')
        for key in sorted(data):
            logging.debug('checking %s', key)
            top = toplevel(key)
            if top not in roots:
                roots.append(top)
            try:
                entry = Entry(key, self.pack, self.opts.rootdir)
            except ValueError:
                error = True
            else:
                logging.debug('%s (%s) %s [%s]', entry.op, entry.dtype, entry.key, entry.ftype)
                if entry.op == 'ignore':
                    logging.info('ignoring %s', entry.key)
                else:
                    entries.append( entry )
        if error:
            raise App.Error


# Opts(args=tuple)
# this is using getopt instead of optparse or argparse as we expect this to be
# run on pre-Python 2.4 systems
class Opts:
    '''Process and capture command-line options and arguments.'''
    shortopts = 'd:DhINqTvVW'
    longopts = [
        'DEBUG', 'directory=', 'diff', 'dump', 'help', 'info',
        'noop', 'quiet', 'test', 'verbose', 'version', 'whitespace'
    ]
    rootdir = App.homedir
    loglevel = logging.WARNING

    def __init__(self, args=()):
        self.args = None
        self.noop = False
        self.showdiff = False
        self.dump = False
        self.doinfo = False
        self.testonly = False
        self.diffwhitespace = True
        self.process_arguments(args or sys.argv[1:])

    # process_arguments(tuple)
    def process_arguments(self, arglist):
        # process arguments (using getopt for older versions of Python2)
        opts, self.args = getopt.getopt(
            arglist,
            self.shortopts,
            self.longopts)
        for opt, arg in opts:
            if opt == '--':
                break
            elif opt in ('--help', '-h'):
                self.do_help()
                raise SystemExit
            elif opt in ('--version', '-V'):
                print 'butler-package.py %s (r%s)' % (ReleaseNo, _revision_num)
                print GENERATED_FOR, GENERATED_WHEN
                raise SystemExit
            elif opt in ('--noop', '-N'):
                self.noop = True
                self.loglevel = logging.INFO
            elif opt in ('--test', '-T'):
                self.noop = True
                self.testonly = True
                self.loglevel = logging.ERROR
            elif opt in ('--diff', '-D'):
                self.noop = True
                self.showdiff = True
            elif opt in ('--dump',):
                self.noop = True
                self.dump = True
            elif opt in ('--info', '-I'):
                self.doinfo = True
            elif opt in ('--whitespace', '-W'):
                self.diffwhitespace = True
            elif opt in ('--directory', '-d'):
                self.rootdir = os.path.normpath(arg)
            elif opt in ('--quiet', '-q'):
                self.loglevel = logging.ERROR
            elif opt in ('--verbose', '-v'):
                self.loglevel = logging.INFO
            elif opt in ('--DEBUG',):
                self.loglevel = logging.DEBUG
            else:
                logging.warn('Uknown option (%s)', opt)

    # do_help() -> None
    def do_help(self):
        print os.path.basename(sys.argv[0]), '[options]'
        print '--help|-h                this information'
        print '--version|-V             version information'
        print '--noop|-N                do not perform operations'
        print '--quiet|-q               fewer messages displayed'
        print '--verbose|-v             more messages displayed'
        print '--DEBUG                  even more messages displayed'
        print '--directory=DIR|-d DIR   install root directory'
        print '--diff|-D                show differences'
        print '--whitespace|-W          ignore whitespace, with --diff'
        print '--dump                   dump files to new directory'
        print '--test|-T                return 0 if no changes'
        print '--info|-I                show file info and patterns (verbose)'


# Pack()
# This is as the end of the script to keep line numbers consistent during
# debugging and for better readability; must be before the __name__ ==
# __main__ section
class Pack(Packet):
    DATA = '''${PICKLEDATA}'''
    ALLOWS = '''${PICKLEALLOWS}'''
    IGNORES = '''${PICKLEIGNORES}'''
    EXCLUDES = '''${PICKLEEXCLUSIONS}'''
    DYNAMICS = '''${PICKLEDYNAMICS}'''


if __name__ == '__main__':
    try:
        app = App()
        app.run()
    except App.Error:
        raise SystemExit(1)
    except App.Exit:
        raise SystemExit(0)
    except IOError:
        if app.opts.loglevel == logging.DEBUG:
            logging.exception('IO Error, terminating prematurely')
        else:
            logging.error('IO Error, terminating prematurely')
        raise SystemExit(1)
