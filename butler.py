#!/usr/bin/python -u
# butler - Michael P. Reilly Copyright 2014-2016 All rights reserved
""" manage the system configurations in the different
environments and networks configurations are:
    .apptools/*.py [.py, .json] .bash/*.sh [.sh] <activemq/config/*>
    config/{appname}/* [.properties, .xml] config/* [.properties,
    .xml] coss-oc4j/config/* [.properties, .xml] coss-tomcat/config/*
    [.properties, .xml, querytemplates/*.xml] Work/.../config/*
    [.properties, .xml]

conceptual design - a generator will take template files and metadata
to generate a "package" (Python script) which will contain the files
to push/update, overwriting any changes there, backups will be taken
beforehand. this 'package' could be executed on the server by:
   ssh {hostname} python -u - < {package}

database structure
   data/
      metadata/
          hosts/
              {user@host.domain}.json    - text/json
              {user@ipaddr}.json         - text/json
          imports/
              {import}.json              - text/json
      templates/
          {product}/
              default/
                  path/to/template - text/plain, application/binary, symlink
              {env}/
                  path/to/template - text/plain, application/binary, symlink
              {userhost}/
                  path/to/template - text/plain, application/binary, symlink
metadata json {
  "product": {product},                # e.g. 'cc-eeb'
  "product": [{product}, {product}],   # e.g. ['cc-ieb', 'cc-mm2']
  "env": {svrenv}-{svrnet},            # e.g. 'test-bos'
  "import": {metadata}                 # e.g. 'cc-eeb_test-bos'
  "import': [{metadata}, {metadata}],  # e.g. ['cc-ieb_test', 'cc-mm2_test']
  "values": {
       {identifier}: {replacementvalue}
  }
  "exclude": [{pattern}]               # e.g. '*/logging.xml'
  "ignore": [{path}]                   # directory, e.g. '.bash/backup'
  "allow": [{path}]                    # directory, e.g. '.bash'
  "dynamic": [{path}]                  # e.g. '*/server.xml'
  "replace": [{repr}]                  # sed-like s/// statement, without 's'
  "binary": [{pattern}]                # e.g. '*.png'
}

The location of the database is determined as first found of:
 * {--directory}/butler
 * {--directory}/data
 * {--directory}/
 * {$BUTLER_DIR}/butler
 * {$BUTLER_DIR}/data
 * {$BUTLER_DIR}/
 * {$PWD}/butler
 * {$PWD}/data
 * {$PWD}/
 * {$HOME}/butler
 * {$HOME}/data
 * {$HOME}/

An optional config file, ~/.butler.py, or specified by the --config option,
can contain the following values:
 * cr_required_for - a Python list of strings, environments where a CR number
   is required.  The remote package for these environments would fail without
   a CR number (--cr option).
"""

try:
    # raise ImportError  # for testing optparse
    from argparse import ArgumentParser
    OptionParser = None
except ImportError:
    from optparse import OptionParser
    ArgumentParser = None
try:
    import json
except ImportError:
    json = None

import errno
import fnmatch
import itertools
import os
import logging
import re
import subprocess
import sys
import time

ReleaseNo = '$ReleaseNo$'
_revision_num = '$Revision$'

Description = """\
If not supplied on the command-line, Butler will read stdin for a sequence of userhost values, each on
its own line.
"""


# Location - a simple structure for containing global values
# such as directories, environment variables.
# there are no methods and no instantiations, just class members
class Location:
    # possible names of the database directory
    database_names = ('butler', 'data', '.')
    homedir = os.path.expanduser('~')
    whereami = os.getcwd()
    config = os.path.join(homedir, '.butler.py')
    # value of --directory option
    database = None
    # value of BUTLER_DIR envvar
    envvar_dir = None
    if 'BUTLER_DIR' in os.environ:
        envvar_dir = os.path.expanduser(os.environ['BUTLER_DIR'])

# Load a python script as a configuration file.
class Config(object):
    def __init__(self, filename):
        try:
            self.values = {'__builtins__': {}}
            execfile(filename, self.values)
        except IOError:
            logging.info('No configuration file found')
        else:
            logging.info('Loaded configs from %s', filename)
            logging.debug('config = %s', self.values)
        try:
            del self.values['__builtins__']
        except KeyError:
            pass
    def __getitem__(self, key):
        return self.values[key]
    def __len__(self):
        return len(self.values)
    def __repr__(self):
        return repr(self.values)


# A variation on the dict, where order added
# is kept, or the keys can be resorted.
# The full dict interface is not needed here.
class OrderedDict(object):
    """An order collection indexed by a hashable object. The order is as it is
added.  Sort will change the sort based on the key."""
    def __init__(self):
        self.l = []
        self.d = {}

    def __contains__(self, key):
        return key in self.d

    def __len__(self):
        return len(self.d)

    def __getitem__(self, key):
        if isinstance(key, int):
            return self.l[key]
        else:
            return self.d[key]

    def __setitem__(self, key, value):
        if key not in self.l:
            self.l.append(key)
        self.d[key] = value

    def __delitem__(self, key):
        if key in self.d:
            del self.d[key]
        else:
            raise KeyError(key)
        self.l.remove(key)

    def sort(self):
        self.l.sort()


# BinTemplate - create an interface for a string substitution
# template.  This class performs no substituion.
class BinTemplate(object):
    """Give a template interface, but assume a binary file."""
    def __init__(self, template):
        self.template = template
        self._initialize()

    # to be subclassed
    @classmethod
    def _initialize(cls):
        pass

    # to be subclassed
    def _subst(self, tempstr):
        return tempstr

    def substitute(self, mapping, **kws):
        self.mapping = mapping
        return self._subst(self.template)
    safe_substitute = substitute


# StrTemplate - substitute tokens in a file with values in a
# dict structure
class StrTemplate(BinTemplate):
    """Templating based on a ${...} token, with conditional interpretation."""
    delim = r'\$'
    id = r'[_a-zA-Z][_a-zA-Z0-9]*'
    # $$
    # ${identifier}
    # ${identifier::string::}  # does not work at this time
    # ${identifier::string::string::}
    patt = r'''
        %(delim)s(?:
            #{(?P<named>%(id)s)(::(?P<condif>.*)(::(?P<condel>.*)?)::)?} |
            {(?P<named>%(id)s)(::(?P<condif>.*)::)?} |
            (?P<escaped>%(delim)s) |
            (?P<invalid>)
        )
    ''' % {'id': id, 'delim': delim}
    patt_re = None

    # _initialize(cls) -> None  -- classmethod
    # create the regular expression as a class instance
    # exceptions - none
    @classmethod
    def _initialize(cls):
        if cls.patt_re is None:
            import re
            cls.patt_re = re.compile(cls.patt, re.VERBOSE)

    # _convert(matchobj) -> str  -- callback
    # return substitution string
    # exceptions - none
    def _convert(self, matchobj):
        # process a match and return the replacement string
        named = matchobj.group('named')
        condif = matchobj.group('condif')
        condel = None  # matchobj.group('condel')
        # print repr(named), repr(condif), repr(condel)
        if matchobj.group('escaped'):
            return self.delim + self.delim
        elif condel is not None and named not in self.mapping:
            return self._subst(condel)
        elif condif is not None and named in self.mapping:
            return self._subst(condif)
        elif condif is not None:
            return ''
        elif named is not None and named in self.mapping:
            return '%s' % (self.mapping[named],)
        else:
            return matchobj.string[matchobj.start(0):matchobj.end(0)]

    # _subst(str) -> str
    # run convert against the passed string
    def _subst(self, tempstr, maxdepth=10):
        # keep converting the template string until there are no more changes
        count = 0
        # this handles circular references
        while count < maxdepth:
            #logger.debug('_subst[%d]', count)
            result = self.patt_re.sub(self._convert, tempstr)
            if result == tempstr:
                break
            tempstr = result
            count += 1
        return result


# find(str) -> generator
# traverse the tree, ignoring version control and cruft files
# stripping the rootdir from the output.
def find(rootdir, ignore=('.svn', '.hg', '.git')):
    """A generator to traverse a directory tree, ignoring certain directories.
"""
    c = os.path.commonprefix([os.path.join(rootdir, f) for f in ('a', 'b')])
    for (dirpath, dirnames, filenames) in os.walk(rootdir):
        for fn in ignore:
            if fn in dirnames:
                dirnames.remove(fn)
        for fn in filenames:
            # VIM swap file... ignore; a) don't want, b) binary
            if fn.startswith('.') and fn.endswith('.swp'):
                continue
            if fn.endswith('~'):  # same with emacs save files
                continue
            f = os.path.join(dirpath, fn)
            yield f.replace(c, '')


# encode_data(str) - str
# pack the data for inclusion in the "package.py" script
def encode_data(data):
    """Pickle, compress and encode the data to be packaged."""
    import base64
    import pickle
    import zlib
    return base64.b64encode(zlib.compress(pickle.dumps(data)))


# VersionControl(str)
# determine which version control system being used
# and retrieve the version string including branch
class VersionControl(object):
    vctypes = (('.hg', 'mercurial'), ('.svn', 'subversion'))

    def __init__(self, rootdir):
        self.revision = ''
        self.branch = ''
        self.rootdir = rootdir
        dir, vct = self.findtype(self.rootdir)
        if dir is not None:
            self.rootdir = dir
            self.vctype = vct
        else:
            self.vctype = 'none'
        self.gather_info()
        if self.revision and self.branch:
            self.vcstr = '%s@%s' % (self.branch, self.revision)
        elif self.revision:
            self.vcstr = self.revision
        elif self.branch:
            self.vcstr = self.branch
        else:
            self.vcstr = ''

    # findtype(str, str) -> {str|None}
    # find the top-level directory of the workspace
    @classmethod
    def findtype(cls, rootdir):
        cwd = os.path.realpath(rootdir) or os.getcwd()
        while cwd not in ('', os.curdir, os.sep):
            for (vcdir, vctype) in cls.vctypes:
                if os.path.isdir(os.path.join(cwd, vcdir)):
                    return (cwd, vctype)
            cwd = os.path.dirname(cwd)
        return (None, None)

    # gather_info() -> None
    # call the matho for vctype
    def gather_info(self):
        assert self.vctype in ('none', 'mercurial', 'subversion'), \
            'unable to determine version control type'
        meth = getattr(self, 'gather_info_%s' % self.vctype)
        meth()

    # gather_info_none() -> None
    # if there is no version control
    def gather_info_none(self):
        pass

    # gather_info_mercurial() -> None
    # if a .hg structure is found
    def gather_info_mercurial(self):
        cmd = ('hg', 'identify', '--id')
        self.revision = self.__call(cmd, cwd=self.rootdir)
        cmd = ('hg', 'identify', '--branch')
        self.branch = self.__call(cmd, cwd=self.rootdir)

    # gather_info_subversion() -> None
    # if a .svn structure is found
    def gather_info_subversion(self):
        cmd = ('svn', 'info')
        stdout = self.__call(cmd, cwd=self.rootdir)
        fields = dict(
            (l.rstrip().split(': ', 1) for l in stdout.split('\n') if l)
        )
        try:
            repos = fields['Repository Root']
            url = fields['URL']
            self.branch = url[len(repos):]
            self.revision = fields['Revision']
        except KeyError:
            pass

    # __call(cmd) -> str  -- staticmethod
    # spawn a process and return the stdout output
    @staticmethod
    def __call(cmd, cwd=None):
        p = subprocess.Popen(cmd,
                             cwd=cwd,
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        (stdout, stderr) = p.communicate()
        if p.returncode != 0:
            logger.warn('Warning: %s error: %s' % (cmd[0], stderr))
        return stdout.rstrip()


# Host(userhost)
class Host(object):
    """Represent a SSH user@host specifier."""
    def __init__(self, userhost):
        if '@' in userhost:
            self.user, self.host = userhost.split('@')
        else:
            self.user, self.host = '', userhost

    def __str__(self):
        if self.user:
            return '%s@%s' % (self.user, self.host)
        else:
            return str(self.host)


# FileCache()
# cache the contents of a file, helpful for multiple
# accesses of the same file
# interface is a (very) simple dict
# but keys are populated on access
class FileCache(object):
    """Cache the contents of files."""
    def __init__(self):
        self.d = {}

    def __len__(self):
        return len(self.d)

    def __contains__(self, key):
        return key in self.d

    # exception IOError
    def __getitem__(self, filename):
        if filename not in self:
            self.d[filename] = open(filename, 'rt').read()
        return self.d[filename]


# DatumFile(str, str)
# a leaf in the template tree
# an interface for handling text replacement of a file
# or symlink
class DatumFile(object):
    def __init__(self, name, filename):
        self.name = name
        self.filename = filename
        if not os.path.isfile(self.filename) and \
           not os.path.islink(self.filename):
            raise IOError(errno.ENOENT, 'no such file', self.filename)
        self.contents = self._get_contents(self.filename)
        self.t = None
        self.new = None

    # assign a binary template
    def binary(self):
        logger.debug('marking %s as binary', self.name)
        self.t = BinTemplate(self.contents)

    # assign a string template
    def string(self):
        self.t = StrTemplate(self.contents)

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return self.name == other

    def __lt__(self, other):
        return self.name < other

    # update(dict) -> None
    # perform the text replacement
    def update(self, values):
        assert self.t is not None, "must call binary or string method first"
        try:
            self.new = self.t.safe_substitute(values)
        except UnicodeDecodeError:
            t, e, tb = sys.exc_info()
            raise t, ("%s: %s" % (self.filename, e)), tb
        else:
            self.new = self.new.replace(r'\$\$', '$$')
    # replace(str) -> None
    # handle sed-like 's' statement
    def replace(self, repl):
        """Simulate sed's substitution command:
{DELIM}RE{DELIM}REPL[{DELIM}]
Later there may be support for options after the last delimitor.
"""
        # only replace if not a binary file
        if isinstance(self.t, StrTemplate):
            delim, rest = repl[:1], repl[1:]
            parts = rest.split(delim, 2)
            self.new = re.sub(parts[0], parts[1], self.new)

    # show_diff() -> None
    # output a diff of the old and new contents
    def show_diff(self):
        import difflib
        c = [s+'\n' for s in self.contents.split('\n')]
        n = [s+'\n' for s in self.new.split('\n')]
        sys.stdout.writelines(difflib.context_diff(c, n))


class Template(DatumFile):
    type = 'file'
    product = None
    env = None

    def _get_contents(self, filename):
        return GFC[filename]


class Symlink(DatumFile):
    type = 'symlink'
    product = None
    env = None

    def _get_contents(self, filename):
        return os.readlink(filename)


# Subdatabase(str)
# the superclass for handling the 'metadata' and
# the 'templates' structures
class Subdatabase(object):
    def __init__(self, dir):
        self.dir = dir
        assert os.path.isdir(self.dir), (self.__class__.__name__, dir)

    def __str__(self):
        return str(self.name)

    def __getitem__(self, key):
        name = self.translate(key)
        logger.debug('finding %s in %s', name, self.dir)
        fname = os.path.join(self.dir, name)
        if not os.path.exists(fname) or \
           (fname.startswith('.') and fname.endswith('.swp')):
            raise KeyError(self.__class__.__name__, key)
        return self.get(fname)

    def translate(self, key):
        return str(key)

    def __iter__(self):
        return iter([f for f in sorted(os.listdir(self.dir))
                     if not f.endswith('.swp')])


# MetaCategory(str)
# handles 'metadata' and 'metadata/{dir}' searches
# allows finding 'metadata/label.json',
# 'metadata/hosts/label.json' or 'metadata/import/label.json'
class MetaCategory(Subdatabase):
    def __init__(self, dir, name='.'):
        super(MetaCategory, self).__init__(dir)
        self.name = name
        self.cats = [Metadata(dir, name)]
        for name in self:
            sdir = os.path.join(dir, name)
            if os.path.isdir(sdir):
                self.cats.append(Metadata(sdir, name))

    # __getitem__(key) -> Metadata
    # return first Metadata instance searching in
    # each category
    def __getitem__(self, key):
        logger.debug('finding %s in %s', key, self.name)
        for cat in self.cats:
            try:
                return cat[key]
            except KeyError:
                pass


# Metadata(str)
# find the json file
class Metadata(Subdatabase):
    def __init__(self, dir, name):
        super(Metadata, self).__init__(dir)
        self.name = name

    # get(str) -> Values
    # load the json file
    def get(self, fname):
        name = os.path.normpath(
            os.path.join(
                self.name,
                os.path.basename(fname)
            )
        )
        try:
            d = json.loads(GFC[fname], encoding='utf-8')
        except ValueError:
            t, e, tb = sys.exc_info()
            raise t, ("%s: %s" % (name, e)), tb
        try:
            self.verify(d, fname)
        except UnicodeDecodeError:
            t, e, tb = sys.exc_info()
            raise t, ('%s: %s' % (name, e)), tb
        logger.info('loading %s', fname)
        return Values(d, os.path.splitext(name)[0])

    # translate(str) -> str
    # we're looking for json files
    def translate(self, key):
        return '%s.json' % key

    # verify(data, fname) -> None
    # check that all data is UTF-8 encodeable
    # exceptions: ValueError, UnicodeDecoreError
    def verify(self, data, fname):
        if not isinstance(data, dict):
            raise ValueError('expecting object as top-level in %s' % fname)
        for key in data:
            key.encode('utf-8')
            if isinstance(data[key], list):
                for i in data[key]:
                    i.encode('utf-8')
            elif isinstance(data[key], dict):
                for k in data[key]:
                    k.encode('utf-8')
                    data[key][k].encode('utf-8')
            else:
                data[key].encode('utf-8')


# Value(str, object, str)
class Value(object):
    def __init__(self, name, data, parent):
        self.name = str(name)
        self.data = data
        self.parent = parent

    def __str__(self):
        return str(self.data)

    def __repr__(self):
        return str(self)
        #return '(%s)%s: %s' % (
        #    str(self.parent), repr(self.name), repr(self.data))

    def __eq__(self, other):
        return self.data == other
    def __lt__(self, other):
        return self.data < other


# Values(dict, str)
# emulate a dict, but values are Value instances
# or dicts of Value instances
class Values(object):
    def __init__(self, data=None, parent=None):
        self.d = {}
        self.parent = parent
        if data is not None:
            self.update(data)

    def __str__(self):
        return str(self.d)

    def __repr__(self):
        return repr(self.d)

    def __iter__(self):
        return iter(self.d)

    def __len__(self):
        return len(self.d)

    def __contains__(self, key):
        return key in self.d

    def __getitem__(self, key):
        return self.d[key]

    # __setitem__(str, str) -> None
    # if the value is a dict, then turn each value into a Value instance
    # if not, then turn the value into a Value instance
    def __setitem__(self, key, value):
        if isinstance(value, dict):
            self.d[key] = Value(key, {}, self.parent)
            for skey in value:
                self.d[key].data[skey] = Value(skey, value[skey], self.parent)
        else:
            self.d[key] = Value(key, value, self.parent)

    def __delitem__(self, key):
        del self.d[key]

    def update(self, other):
        if isinstance(other, dict):
            for key in other:
                self[key] = other[key]
        elif isinstance(other, (tuple, list)):
            for key, val in other:
                self[key] = other[key]
        else:
            raise TypeError(other)

    def copy(self):
        return Values(self.d.copy(), self.parent)


# TemplateRoot(str)
# the top-level of the 'templates' structure
class TemplateRoot(Subdatabase):
    # get(str) -> DatumFile
    def get(self, fname):
        return Product(fname)


# Product(str)
# second tier of 'templates' structure
class Product(Subdatabase):
    # get(str) -> DatumFile
    def get(self, fname):
        return Environment(fname)


# Environment(str)
# third tier of 'templates' structure
class Environment(Subdatabase):
    # __getitem___(str) -> DatumFile
    def __getitem__(self, key):
        fname = os.path.join(self.dir, str(key))
        if os.path.islink(fname):
            return Symlink(str(key), fname)
        elif os.path.isfile(fname):
            return Template(str(key), fname)
        else:
            raise KeyError(key)

    def __iter__(self):
        return find(self.dir)


# Database(str)
# represent the data store, with 'metadata' and
# 'templates' structures
class Database(object):
    structure = (
        ('metadata', MetaCategory, 'metadata'),
        ('templates', TemplateRoot, 'templates'),
    )

    def __init__(self, rootdir):
        self.rootdir = os.path.normpath(rootdir)
        if not os.path.isdir(self.rootdir):
            raise IOError(errno.ENOENT, 'no such directory', self.rootdir)
        for name, cls, dname in self.structure:
            dir = os.path.join(self.rootdir, dname)
            if not os.path.isdir(dir):
                raise IOError(errno.ENOENT, 'no such directory', dir)
            setattr(self, name, cls(dir))
        logger.debug('database at %s', rootdir)


# Data()
# represent the data from a metadata file
class Data(object):
    def __init__(self):
        self.product = None
        self.env = None
        self.values = {}
        self.excludes = []
        self.allows = []
        self.ignores = []
        self.dynamics = []
        self.replaces = []
        self.binaries = []
        self.imports = {}

    # vset(str, object) -> None  -- callback
    def vset(self, attrname, value):
        """If unset, then set with the value, if a list, append, otherwise,
make a list of the existing and new values."""
        v = getattr(self, attrname)
        if v is None:
            setattr(self, attrname, value)
        elif isinstance(v, list):
            if isinstance(value, list):
                for i in value:
                    if i not in v:
                        v.insert(0, i)
            elif value not in v:
                v.insert(0, value)
        elif isinstance(value, list):
            setattr(self, attrname, [v])
            v = getattr(self, attrname)
            for i in value:
                if i not in v:
                    v.insert(0, i)
        elif isinstance(value.data, list):
            setattr(self, attrname, [v])
            v = getattr(self, attrname)
            for i in value.data:
                if i not in v:
                    v.insert(0, i)
        elif v != value:
            logger.debug("vset: changing %s from string to list [%s, %s]",
                         attrname, value, v)
            setattr(self, attrname, [value, v])

    # vupdate(str, object) -> None  -- callback
    def vupdate(self, attrname, value):
        """It's a dictionary, so let it happen."""
        getattr(self, attrname).update(value.data)

    # vextend(str, object) -> None  -- callback
    def vextend(self, attrname, value):
        """Add only unique items (similar to set.add)."""
        l = getattr(self, attrname)
        for v in value.data:
            if v not in l:
                l.append(v)

    vmap = {
        'product': ('product', vset),
        'env': ('env', vset),
        'values': ('values', vupdate),
        'excludes': ('exclude', vextend),
        'allows': ('allow', vextend),
        'ignores': ('ignore', vextend),
        'dynamics': ('dynamic', vextend),
        'replaces': ('replace', vextend),
        'binaries': ('binary', vextend),
    }

    # load(str, Database, cache=None) -> None  -- recursive
    # using the vmap member, assign data as found
    # in the json data
    # values in the "import" data are recursively loaded
    # into the cache dict
    # exceptions, ValueError, KeyError
    def load(self, mdname, db, cache=None, imports=None):
        if cache is None:
            cache = {}
        if imports is None:
            imports = self.imports[str(mdname)] = {}
        if mdname in cache:
            # already loaded, do nothing more
            return
        try:
            md = db.metadata[mdname]
        except ValueError:  # json syntax error
            t, e, tb = sys.exc_info()
            raise t, "metadata %s: %s" % (mdname, e), tb
        except KeyError:
            raise ValueError('no metadata for %s' % mdname)
        else:
            cache[mdname] = md
            logger.info('loading %s %s', mdname, md)
        if md is None:
            raise ValueError('unable to retrieve metadata for %s' % mdname)
        if 'import' in md:
            v = md['import']
            if isinstance(v.data, list):
                for n in v.data:
                    imports[n] = {}
                    self.load(n, db, cache, imports[n])
            else:
                imports[v.data] = {}
                self.load(v.data, db, cache, imports[v.data])
        for attrname in self.vmap:
            (dname, ubmeth) = self.vmap[attrname]
            if dname in md:
                ubmeth(self, attrname, md[dname])


# Package(str, Database, Host, Opts, str)
class Package(object):
    """Encapsulate a package, a script which will run on the userhost server
and update the configuration files on that server."""
    script_filename = 'package.py'
    outputfile = 'pack-%(name)s.py'  # default filename pattern
    distdir = 'dist'

    def __init__(self, userhost, db, remhost, opts, config, vcstr):
        self.userhost = userhost
        self.db = db
        self.remhost = remhost
        self.values = {}
        self.now = time.strftime('%c')
        self.opts = opts
        self.config = config
        self.vcstr = vcstr
        self.data = None
        self.templates = None

    # run(str) -> None
    # find the metadata and template files
    # output the info
    # generate the package
    # emit the package or handle remote operation
    def run(self, operation):
        logger.info('gathering for %s', self.userhost)
        self.find_values()
        if self.opts.showimports:
            self.show_import_hierarchy()
            return
        elif self.opts.findimport:
            self.find_import_in_hierarchy(self.opts.findimport)
            return
        elif self.opts.showdiff:
            return  # handle externally to diff multiple

        if self.opts.showval:
            logcall = logger.error
            logcall('=%s=', self.userhost)
        else:
            logcall = logger.debug
        for key in sorted(self.data.values):
            if self.opts.verbose == logging.ERROR:  # --quiet
                logcall('V %s = %s', key, self.data.values[key])
            else:
                logcall('value [%s]%s = %s', self.data.values[key].parent, key,
                        self.data.values[key])
        for t in sorted(self.templates):
            tv = self.templates[t]
            if self.opts.verbose == logging.ERROR:  # --quiet
                logcall('T %s', tv.name)
            elif isinstance(tv.t, StrTemplate):
                logcall('template [%s/%s]%s', tv.product, tv.env, tv.name)
            else:
                logcall('template (%s/%s)%s', tv.product, tv.env, tv.name)
        if self.opts.showval:
            return
        pack = self.generate(self.templates, self.data)
        if operation == 'write':
            try:
                self.write(pack)
            except IOError, e:
                raise ValueError(e)
        else:
            self.remote(operation, pack)

    def find_import_in_hierarchy(self, findstr, d=None, root=None):
        if findstr.startswith('@'):  # interpret as regular expression
            import re
            rexp = re.compile(findstr[1:])
            logger.debug('regexp %s', findstr[1:])
        else:
            from fnmatch import fnmatchcase
            rexp = None
            logger.debug('glob %s', findstr)

        if d is None:
            d = self.data.imports
        for i in d:
            logger.debug('find_import of %s against %s', i, findstr)
            if rexp and rexp.search(str(i)):
                print root or i, i
            elif rexp is None and fnmatchcase(str(i), findstr):
                print root or i, i
            self.find_import_in_hierarchy(findstr, d[i], root or i)

    def show_import_hierarchy(self):
        def showvals(d, indent):
            for i in d:
                print '  ' * indent + str(i)
                showvals(d[i], indent+1)
        showvals(self.data.imports, 0)

    # find_values() -> OrderDict, Data
    # recursively load the json files from the metadata structure
    # find all the templates based on the products and envs values
    def find_values(self):
        uh = self.userhost
        self.data = data = Data()
        data.load(uh, self.db)
        logger.debug('loaded metadata for %s', uh)
        if data.product is None:
            raise ValueError('unable to find product in metadata for %s' % uh)
        if data.env is None:
            raise ValueError('unable to find env in metadata for %s' % uh)
        logger.info('product = %s', data.product)
        logger.info('env = %s', data.env)
        logger.info('%d values found for %s', len(data.values), uh)
        # logger.info('values = %s', data.values)
        logger.info('excludes = %s', data.excludes)
        logger.info('allows = %s', data.allows)
        logger.info('ignores = %s', data.ignores)
        logger.info('binaries = %s', data.binaries)
        logger.info('imports = %s', data.imports)
        # now that we have all the pieces, get the templates
        self.templates = templates = OrderedDict()
        if isinstance(data.product, list):
            for p in data.product:
                logger.debug('product = %s', p)
                self.gather_templates(uh, p, data.env, templates)
        else:
            logger.debug('product = %s', data.product)
            self.gather_templates(uh,
                                  data.product, data.env, templates)
        templates.sort()
        # replace values in the template
        for t in templates:
            tv = templates[t]
            # check if we have a binary file or string
            for patt in data.binaries:
                if fnmatch.fnmatchcase(t, patt):
                    tv.binary()
                    break
            else:
                tv.string()
            tv.update(data.values)
            for repl in data.replaces:
                tv.replace(repl)

    # gather_templates(Host, object, object, OrderedDict) -> None
    # looking in the given environments as well as 'default' and
    # the Host value in most to least specific order, find the
    # templates needed
    def gather_templates(self, userhost, product, env, templates):
        toscan = [userhost]
        if isinstance(env, list):
            toscan.extend(env)
        else:
            toscan.append(env)
        toscan.append('default')
        for env in toscan:
            try:
                troot = self.db.templates[product][env]
            except KeyError:
                pass
            else:
                logger.debug('%s templates: %s', env, list(troot))
                for tname in troot:
                    if tname not in templates:  # avoid dups
                        tval = troot[tname]
                        # store the product and env for logging later
                        tval.product = product
                        tval.env = env
                        templates[tval.name] = tval
                    else:
                        logger.info('occluding [%s/%s]%s', product, env, tname)

    # generate(OrderedDict, Data) -> str
    # textually replace encoded values into the "package.py"
    # script, as well as non-encoded string versions
    def generate(self, templates, data):
        d = {}
        for t in templates:
            tv = templates[t]
            d[tv.name] = (tv.type, tv.new)
        filelist = [templates[i].name.encode('utf-8') for i in templates]
        allows = [t.encode('utf-8') for t in data.allows]
        ignores = [t.encode('utf-8') for t in data.ignores]
        exclusions = [t.encode('utf-8') for t in data.excludes]
        dynamics = [t.encode('utf-8') for t in data.dynamics]
        binaries = [t.encode('utf-8') for t in data.binaries]
        try:
            cr_req_env = self.config['cr_required_for']
        except KeyError:
            is_cr_req = False
        else:
            envval = set()
            crval = set()
            if isinstance(self.data.env, Value):
                envval.add(self.data.env.data)
            else:
                envval.update(set([self.data.env[k].data for k in self.data.env]))
            if isinstance(cr_req_env, str):
                crval.add(cr_req_env)
            elif isinstance(cr_req_env, (tuple, list)):
                crval.update(set(cr_req_env))
            else:
                logging.warn('invalid data for "cr_required_for"')
            is_cr_req = len(crval.intersection(envval)) > 0
        if self.opts.cr is None:
            crvalue = ''
        else:
            crvalue = self.opts.cr.encode('utf-8')
        logging.info('is_cr_required = %s', is_cr_req)
        logging.info('crvalue = %s', crvalue)
        substdata = {
            'DB_VERSION': self.vcstr,
            'GENERATED_WHEN': self.now,
            'GENERATED_FOR': self.userhost,
            'CHANGE_REQ': crvalue,
            'IS_CR_REQ': (is_cr_req and 'True' or 'False'),
            'PICKLEDATA': encode_data(d),
            'FILELIST': ', '.join(sorted(filelist)),
            'ALLOWS': ', '.join(allows),
            'PICKLEALLOWS': encode_data(allows),
            'IGNORES': ', '.join(ignores),
            'PICKLEIGNORES': encode_data(ignores),
            'EXCLUSIONS': ', '.join(exclusions),
            'PICKLEEXCLUSIONS': encode_data(exclusions),
            'DYNAMICS': ', '.join(dynamics),
            'PICKLEDYNAMICS': encode_data(dynamics),
            'BINARIES': ', '.join(binaries),
            'PICKLEBINARIES': encode_data(binaries),
        }
        t = StrTemplate(Package_py)
        return t.safe_substitute(substdata)

    # write(str) -> None
    # write the package string to an appropriate named file
    def write(self, pack):
        if self.opts.filename:
            filename = self.opts.filename
        else:
            filename = os.path.join(self.distdir, self.outputfile)
            if not os.path.isdir(self.distdir):
                try:
                    os.mkdir(self.distdir)
                except OSError:
                    logger.error('unable to create %s', repr(self.distdir))
                    return
        # substitute userhost for %(name)s
        filename = filename % {'name': str(self.userhost)}
        if os.path.exists(filename):
            logger.warning('writing to %s', filename)
        else:
            logger.warning('creating %s', filename)
        with open(filename, 'wt') as ofile:
            ofile.write(pack)
        try:
            os.chmod(filename, int('0755', 8))
        except OSError:
            logger.warn('unable to change perms on %s' % filename)

    # remote(str, str) -> None
    # call python over an SSH connection, sending the
    # package string as stdin with appropriate options
    def remote(self, mode, pack):
        if self.remhost is not None:
            userhost = self.remhost
        else:
            userhost = self.userhost
        if mode != 'test':
            logger.warning('remote call to %s', userhost)
        cmd = (
            'ssh',
            '-o', 'ConnectTimeout=4', '-o', 'BatchMode=yes',
            '-o', 'ForwardAgent=no', '-o', 'ForwardX11=no',
            '-o', 'GSSAPIAuthentication=no', '-o', 'LogLevel=ERROR',
            '-o', 'PasswordAuthentication=no',
            '-o', 'StrictHostKeyChecking=no',
            str(userhost),
            'python', '-u', '-'
        )
        if mode == 'noop':
            cmd += ('--noop',)
        elif mode == 'test':
            cmd += ('--test',)
        elif mode == 'diff':
            cmd += ('--diff',)
        elif mode == 'diffW':
            cmd += ('--diff', '--whitespace')
        # else mode in ('remote', 'push')
        elif mode not in ('remote', 'push'):
            raise ValueError('invalid operation: %s' % mode)
        if logger.getEffectiveLevel() == logging.DEBUG:
            cmd += ('--DEBUG',)
        elif logger.getEffectiveLevel() in (logging.DEBUG, logging.INFO):
            cmd += ('--verbose',)
        logger.debug('cmd = %s', cmd)
        p = subprocess.Popen(cmd, stdin=subprocess.PIPE)
        (o, e) = p.communicate(pack)
        rc = p.wait()
        if rc and self.opts.operation != 'test':
            raise ValueError('Error on remote call')
        elif rc:
            logger.error(userhost)


# App(args) -> None
# for each userhost on the command-line or stdin
# generate a package for it
class App(object):
    # read arguments from the command-line or from stdin
    # determine the datastore directory and open it
    # determine its version control info
    def __init__(self, args=None):
        self.hosts = self.process_args(args)
        if len(self.hosts) == 0 and not sys.stdin.isatty():
            self.hosts = [
                Host(l.strip().split()[0]) for l in sys.stdin
            ]
        elif len(self.hosts) == 0:
            self.hosts = []
        logger.setLevel(self.opts.verbose)
        self.config = Config(self.opts.config)
        dirset = (
            Location.database,
            Location.envvar_dir,
            Location.whereami,
            Location.homedir,
        )
        # permutations of dirset and Location.database_names
        dirs = [
            [
                os.path.join(pdir, sdir)
                for sdir in Location.database_names
            ] for pdir in dirset if pdir
        ]
        for dbdir in itertools.chain(*dirs):
            try:
                self.db = Database(dbdir)
            except IOError:
                logger.debug('no database at %s', dbdir)
            else:
                break
        else:
            raise IOError(errno.ENOENT, 'no database found')
        vc = VersionControl(self.db.rootdir)
        logger.debug('vc type = %s', vc.vctype)
        self.vcstr = vc.vcstr

    # run() -> bool
    # Generate a pack for each incoming host and "run" it
    def run(self):
        success = True
        packages = [
            Package(uh, self.db, self.opts.host, self.opts, self.config,
                    self.vcstr)
            for uh in self.hosts
        ]
        for pack in packages:
            try:
                pack.run(self.opts.operation)
            except ValueError, e:
                logger.error(e)
                success = False
                # continue with the next package
            except KeyboardInterrupt:
                return False
        if self.opts.showdiff:
            self.showdiff(packages)
        return success

    def showdiff(self, packages):
        values = set()
        files = set()
        # this seems longer and more wasteful, but we need to know all
        # the value and template names before we process so we can also
        # determine which packages do not have those names, which would
        # also be a difference
        for pack in packages:
            logger.debug('pack = %s', pack)
            if pack.data is not None:
                for value in pack.data.values:
                    logger.debug('value = %s', value)
                    values.add(value)
            if pack.templates is not None:
                for fname in pack.templates:
                    files.add(fname)
        # for each metadata name, we want to get the value, or None
        metadata = {}
        for name in values:
            entry = []
            for pack in packages:
                if pack.data and name in pack.data.values:
                    entry.append( (pack, pack.data.values[name]) )
                else:
                    entry.append( (pack, None) )
            metadata[name] = entry
        # for templates, we are not conserned about the contents being
        # different but the location being different (prod,env vs prod,env)
        templates = {}
        for name in files:
            entry = []
            for pack in packages:
                if pack.templates and name in pack.templates:
                    entry.append( (pack, pack.templates[name]) )
                else:
                    entry.append( (pack, None) )
            templates[name] = entry
        for name in sorted(metadata):
            s = set(f[1] and f[1].data or None for f in metadata[name])
            if len(s) > 1:
                logger.error('--V-%s-----', name)
                for pack, item in metadata[name]:
                    if item is None:
                        logger.error('%s: None', pack.userhost)
                    else:
                        logger.error('%s: %s.%s=%s', pack.userhost, item.parent,
                                                item.name, item.data)
        for name in sorted(templates):
            s = set(f[1] and (str(f[1].product), str(f[1].env)) or None for f in templates[name])
            if len(s) > 1:
                logger.error('--T-%s-----', name)
                for pack, item in templates[name]:
                    if item is None:
                        logger.error('%s: None', pack.userhost)
                    else:
                        logger.error('%s: %s.%s=%s', pack.userhost, item.product,
                                                item.env, item.name)

    # process_args(list) -> list
    # process the options and arguments
    # if there are any arguments, return a list of Host instances
    # update the Location database member
    def process_args(self, args):
        opts = argparser.parse_args(args)
        if isinstance(opts, tuple):
            opts, args = opts
        else:
            args = opts.userhosts
        self.opts = opts
        if opts.directory:
            Location.database = os.path.normpath(opts.directory)
        return self.validate_hosts(args)

    # validate_hosts(list) -> list
    def validate_hosts(self, arguments):
        hosts = []
        for i in arguments:
            try:
                host = Host(i)
            except ValueError:
                logger.warn('ignoring, invalid host: %s' % i)
            else:
                hosts.append(host)
        return hosts


# If argparse is available, use that, otherwise use optparse
if ArgumentParser:
    usagestring = '%(prog)s [options] [userhost ...]'
    versionstring = '%%(prog)s %s (r%s)' % (ReleaseNo, _revision_num)
    argparser = ArgumentParser(description=Description,
                               usage=usagestring)
    argparser.add_argument('userhosts', nargs='*',
                           help='one or more SSH style user@host strings')
    argparser.add_argument('--version', action='version',
                           version=versionstring)
    add_opt = argparser.add_argument
else:
    usagestring = '%prog [options] [userhost ...]'
    versionstring = '%%prog %s (r%s)' % (ReleaseNo, _revision_num)
    argparser = OptionParser(prog='butler',
                             version=versionstring,
                             usage=usagestring,
                             description=Description)
    add_opt = argparser.add_option
add_opt('--verbose', '-v', dest='verbose',
        action='store_const', const=logging.INFO, default=logging.WARNING,
        help='display more messages')
add_opt('--quiet', '-q', dest='verbose',
        action='store_const', const=logging.ERROR, default=logging.WARNING,
        help='display fewer messages')
add_opt('--DEBUG', dest='verbose',
        action='store_const', const=logging.DEBUG, default=logging.WARNING,
        help='display debugging messages')
add_opt('--config', metavar='FILE',
        default=Location.config,
        help='configuration filename')
add_opt('--host', '-H', metavar='USERHOST',
        help='remote host to connect to')
add_opt('--filename', '-F', metavar='FILE',
        help='filename to write output')
add_opt('--directory', '-d', metavar='DIR',
        help='location of data directory')
add_opt('--show', '-S', dest='showval',
        action='store_true',
        help='display values and templates from datastore')
add_opt('--showdiff', '-D', dest='showdiff',
        action='store_true',
        help='show differences between values in datastore')
add_opt('--showimports', '-I', dest='showimports',
        action='store_true',
        help='display import hierarchy')
add_opt('--findimport', metavar='STR', dest='findimport',
        help='find imported module; preceeding "@" denotes regexp, otherwise a glob')
add_opt('--op', '-o', choices=('write', 'remote', 'noop', 'diff', 'diffW',
                               'push', 'test'),
        default='write', dest='operation',
        help='operation to perform (default: write)')
add_opt('--cr', '-C', action='store',
        help='change request number, when required')

GFC = FileCache()  # global file cache

# There should be _NO_ triple double quotes ("""...""") inside package.py
Package_py = """$PACKAGEPY$"""

if __name__ == '__main__':
    logging.basicConfig(
        format='%(message)s',
        stream=sys.stdout
    )
    logger = logging.getLogger(os.path.splitext(argparser.prog)[0])
    try:
        app = App()
        if not app.run():
            raise SystemExit(1)
    except IOError, e:
        logger.error(e)
        raise SystemExit(1)
    except Exception, e:
        logger.exception(e)
        raise SystemExit(1)
