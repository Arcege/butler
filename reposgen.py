#!/usr/bin/python
# Michael P. Reilly Copyright 2015-2016 All rights reserved
# generate a new Butler repository with examples

import json
import os
import sys

def mkdir(path):
    """Equivalent to mkdir -p."""
    if os.path.isdir(path) or \
       path in (os.sep, os.curdir, ''):
        return
    mkdir(os.path.dirname(path))
    if not os.path.exists(path):
        os.mkdir(path)
    else:
        raise OSError('File already exists: %s' % path)

try:
    repos_dir = sys.argv[1]
except IndexError:
    raise SystemExit('Expected directory of repository')

if not os.path.exists(repos_dir):
    print 'repository', repos_dir
    mkdir(repos_dir)
elif not os.path.isdir(repos_dir):
    raise SystemExit('Expected location is not a directory')
elif not os.access(repos_dir, os.X_OK):
    raise SystemExit('Must have execute perm on repository')
elif not os.access(repos_dir, os.W_OK):
    raise SystemExit('Must have write perm on repository')

md_dir = os.path.join(repos_dir, 'metadata'); mkdir(md_dir)
t_dir = os.path.join(repos_dir, 'templates'); mkdir(t_dir)

# example 1
metadata = {
    'example':
        {'product': 'example'},
    'example1':
        {'import': 'example', 'env': '1',
         'values': {'value': 'spam'}},
    'example2':
        {'import': 'example', 'env': '1',
         'values': {'value': 'eggs'}},
    'example3':
        {'import': 'example', 'env': '2',
         'values': {'value': 'spam'}},
}
templates = {
    os.path.join('example', '1', 'config', 'foo.txt'):
        '''This is an example config file with value ''' + \
        '''substitution.\n Value is ${value}\n''',
    os.path.join('example', '2', 'config', 'a.txt'):
        '''This is environment specific file.\n''',
    os.path.join('example', 'default', 'config', 'bar.txt'):
        '''This is a more global config file.\n''',
    os.path.join('example', 'example1', 'config', 'a.txt'):
        '''This is a system specific config file.''',
}

for fname in metadata:
    print 'metadata', fname
    filename = os.path.join(md_dir, fname + '.json')
    mkdir(os.path.dirname(filename))
    f = open(filename, 'wt')
    json.dump(metadata[fname], f, sort_keys=True, indent=4,
              separators=(',', ': '))
    f.close()

for fname in templates:
    print 'template', fname
    filename = os.path.join(t_dir, fname)
    mkdir(os.path.dirname(filename))
    f = open(filename, 'wt')
    f.write(templates[fname])
    f.close()

