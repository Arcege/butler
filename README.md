# README #

A program to update and synchronize configuration files on remote systems.

Butler works on files in a repository (often under version control).

## Repository setup ##
A butler repository consists of two sections: a 'metadata' and a 'templates' directory.  To start out:

1. Create both 'metadata' and 'templates' directories.
1. Create a JSON file (see DATA.txt); the basename of which would be passed to `butler`; at a minimum, there needs to be the following values defined:
    1. product - used as first tier under 'templates' directory.
    1. env - used as second tier under 'templates' directory.
1. Create directories in 'templates' to match the product and env values.
1. Copy directories and files into the templates/*product*/*env* structure.
1. Add to version control, if desired.

### Who do I talk to? ###

* For support, questions or comments, contact <arcege@gmail.com>

## Licensing ##
Butler is protected under the GNU General Public License.  See LICENSE.txt for more information